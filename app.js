const http = require("http");

const Todo = require("./controller");
const {getReqData} = require("./utils");

const PORT = process.env.PORT || 5001

const server = http.createServer(async (req, res)=>{
    // GET TODO'S - /api/todos

    if(req.url === '/api/todos' && req.method === "GET"){
        try{

        let todos = await new Todo().getTodos()
        res.writeHead(200, {"Content-Type":"application/json"});
        res.write("Hello from Vanilla Node JS Api");
        res.end(JSON.stringify(todos))
    
        }catch (error) {
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message:error}))
        }

    }
    
    // GET TODO - /api/todos/id

    else if(req.url.match(/\/api\/todos\/([0-9]+)/)  && req.method === "GET"){
        try{
            let id = req.url.split("/")[3];
            let todo = await new Todo().getTodo(id)
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(todo))
        }catch(err){
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: err}))
        } 
    }

    //DELETE TODO - /api/todos:id 
    else if(req.url.match(/\/api\/todos\/([0-9]+)/)  && req.method === "DELETE"){
        try{
            let id = req.url.split("/")[3];
            let message = await new Todo().deletingTodo(id)
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(message))
        }catch(err){
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: err}))
        } 
    }


    //UPDATE TODO - /api/todos:id 
    else if(req.url.match(/\/api\/todos\/([0-9]+)/)  && req.method === "PATCH"){
        try{
            let id = req.url.split("/")[3];
            let updated_Todo = await new Todo().updatingTodo(id)
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(updated_Todo))
        }catch(err){
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: err}))
        } 
    }


     //CREATE TODO - /api/todos
     else if(req.url === "/api/todos" && req.method === "POST"){
        try{
            let todo_data = await getReqData(req)
            let new_Todo = await new Todo().createTodo(JSON.parse(todo_data))
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.end(JSON.stringify(new_Todo))
        }catch(err){
            res.writeHead(404, {"Content-Type": "application/json"})
            res.end(JSON.stringify({message: err}))
        } 
    }


    // NO Route Found
    else{
        res.writeHead(404, {"Content-Type": "application/json"})
        res.end(JSON.stringify({message: "Route not found"}))
    }
})

server.listen(PORT, ()=>console.log(`Server started at : ${PORT}`));

