const { resolve } = require("path");
const data = require("./data");

class Controller{
    async getTodos(){
        return new Promise((resolve, _)=> resolve(data))
    }

    async getTodo(id){
        return new Promise((resolve, reject)=>{
            let todo = data.find((todo)=> todo.id === parseInt(id))
            
            if(!todo){
                reject(`todo not found with id ${id}`)
            }
            resolve(todo)
        })
    }

    async createTodo(todo){
         return new Promise((resolve, _)=>{
            let newTodo = {
                id : Math.floor(4 + Math.random()*10),
                ...todo
            }
            
            resolve(newTodo)
         })
    }

    async updatingTodo(id){
        return new Promise((resolve, reject)=>{
            let todo = data.find(todo => todo.id === parseInt(id))
            
            if(!todo){
              reject(`todo not found with id ${id}`)
            }

            todo['completed'] = true

            resolve(todo);
        }) 
    }

    async deletingTodo(id){
        return new Promise((resolve, reject)=>{
            let todo = data.find(todo => todo.id === parseInt(id))
            
            if(!todo){
              reject(`todo not found with id ${id}`)
            }
            resolve(`todo deleted successfully`)
        })
    }
}

module.exports = Controller