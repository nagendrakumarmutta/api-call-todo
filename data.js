const todos = [
    {
        id:1,
        title:"coding in Javascript",
        description:"working on functions in JAVASCRIPT",
        completed:false
    },
    {
        id:2,
        title:"Cooking supper",
        description:"making rice and chicken ready to eat",
        completed:false
    },
    {
        id:3,
        title:"Taking a walk",
        description:"easy time in park with dog",
        completed:false
    }, 
    {
        id:4,
        title:"streaming Youtube",
        description:"streaming youtube videos",
        completed:true
    }
]

module.exports = todos